/* globals angular, Chart*/
var App = angular.module("app", []);

App.controller("dataCtrl", function ($scope, $http) {
	$scope.json = [];
	$scope.months = [];
	$scope.price_m2s = [];

	$scope.data = $http.get('js/data.json')
		.then(function(res){
			var json = angular.fromJson(res.data);
			var titleChart = '\u20AC / m2 Barcelona';

			angular.forEach(json, function(v, k) {
				$scope.json.push({key:k,year:v.year,month:v.month,price_m2:v.price_m2})
				$scope.months.push(v.month);
				$scope.price_m2s.push(v.price_m2);
			});


			new Chart(document.getElementById("line"), {
				type: 'line',
				data: {
					labels: $scope.months,
					datasets: [{ 
						data: $scope.price_m2s,
						label: titleChart,
						borderColor: "rgba(62, 149, 205, 1)",
						backgroundColor: "rgba(62, 149, 205, 0.5)",
						borderWidth: 3,
						fill: true
					}]
				},
				options: {
					title: {
						display: false,
						text: titleChart
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					responsive: true,
					maintainAspectRatio: false,
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: false,
								labelString: 'Month'
							},
							ticks: {
								maxRotation:90,
								minRotation:45
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: false,
								labelString: 'Value'
							},
							ticks: {
								beginAtZero: true,
								stepSize: 1000,
								suggestedMax: 6000
							}
						}]
					}
				}
			});

			return json;     
		});
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjcmlwdHMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6InNjcmlwdHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBnbG9iYWxzIGFuZ3VsYXIsIENoYXJ0Ki9cbnZhciBBcHAgPSBhbmd1bGFyLm1vZHVsZShcImFwcFwiLCBbXSk7XG5cbkFwcC5jb250cm9sbGVyKFwiZGF0YUN0cmxcIiwgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHApIHtcblx0JHNjb3BlLmpzb24gPSBbXTtcblx0JHNjb3BlLm1vbnRocyA9IFtdO1xuXHQkc2NvcGUucHJpY2VfbTJzID0gW107XG5cblx0JHNjb3BlLmRhdGEgPSAkaHR0cC5nZXQoJ2pzL2RhdGEuanNvbicpXG5cdFx0LnRoZW4oZnVuY3Rpb24ocmVzKXtcblx0XHRcdHZhciBqc29uID0gYW5ndWxhci5mcm9tSnNvbihyZXMuZGF0YSk7XG5cdFx0XHR2YXIgdGl0bGVDaGFydCA9ICdcXHUyMEFDIC8gbTIgQmFyY2Vsb25hJztcblxuXHRcdFx0YW5ndWxhci5mb3JFYWNoKGpzb24sIGZ1bmN0aW9uKHYsIGspIHtcblx0XHRcdFx0JHNjb3BlLmpzb24ucHVzaCh7a2V5OmsseWVhcjp2LnllYXIsbW9udGg6di5tb250aCxwcmljZV9tMjp2LnByaWNlX20yfSlcblx0XHRcdFx0JHNjb3BlLm1vbnRocy5wdXNoKHYubW9udGgpO1xuXHRcdFx0XHQkc2NvcGUucHJpY2VfbTJzLnB1c2godi5wcmljZV9tMik7XG5cdFx0XHR9KTtcblxuXG5cdFx0XHRuZXcgQ2hhcnQoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJsaW5lXCIpLCB7XG5cdFx0XHRcdHR5cGU6ICdsaW5lJyxcblx0XHRcdFx0ZGF0YToge1xuXHRcdFx0XHRcdGxhYmVsczogJHNjb3BlLm1vbnRocyxcblx0XHRcdFx0XHRkYXRhc2V0czogW3sgXG5cdFx0XHRcdFx0XHRkYXRhOiAkc2NvcGUucHJpY2VfbTJzLFxuXHRcdFx0XHRcdFx0bGFiZWw6IHRpdGxlQ2hhcnQsXG5cdFx0XHRcdFx0XHRib3JkZXJDb2xvcjogXCJyZ2JhKDYyLCAxNDksIDIwNSwgMSlcIixcblx0XHRcdFx0XHRcdGJhY2tncm91bmRDb2xvcjogXCJyZ2JhKDYyLCAxNDksIDIwNSwgMC41KVwiLFxuXHRcdFx0XHRcdFx0Ym9yZGVyV2lkdGg6IDMsXG5cdFx0XHRcdFx0XHRmaWxsOiB0cnVlXG5cdFx0XHRcdFx0fV1cblx0XHRcdFx0fSxcblx0XHRcdFx0b3B0aW9uczoge1xuXHRcdFx0XHRcdHRpdGxlOiB7XG5cdFx0XHRcdFx0XHRkaXNwbGF5OiBmYWxzZSxcblx0XHRcdFx0XHRcdHRleHQ6IHRpdGxlQ2hhcnRcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdGhvdmVyOiB7XG5cdFx0XHRcdFx0XHRtb2RlOiAnbmVhcmVzdCcsXG5cdFx0XHRcdFx0XHRpbnRlcnNlY3Q6IHRydWVcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHJlc3BvbnNpdmU6IHRydWUsXG5cdFx0XHRcdFx0bWFpbnRhaW5Bc3BlY3RSYXRpbzogZmFsc2UsXG5cdFx0XHRcdFx0c2NhbGVzOiB7XG5cdFx0XHRcdFx0XHR4QXhlczogW3tcblx0XHRcdFx0XHRcdFx0ZGlzcGxheTogdHJ1ZSxcblx0XHRcdFx0XHRcdFx0c2NhbGVMYWJlbDoge1xuXHRcdFx0XHRcdFx0XHRcdGRpc3BsYXk6IGZhbHNlLFxuXHRcdFx0XHRcdFx0XHRcdGxhYmVsU3RyaW5nOiAnTW9udGgnXG5cdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdHRpY2tzOiB7XG5cdFx0XHRcdFx0XHRcdFx0bWF4Um90YXRpb246OTAsXG5cdFx0XHRcdFx0XHRcdFx0bWluUm90YXRpb246NDVcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fV0sXG5cdFx0XHRcdFx0XHR5QXhlczogW3tcblx0XHRcdFx0XHRcdFx0ZGlzcGxheTogdHJ1ZSxcblx0XHRcdFx0XHRcdFx0c2NhbGVMYWJlbDoge1xuXHRcdFx0XHRcdFx0XHRcdGRpc3BsYXk6IGZhbHNlLFxuXHRcdFx0XHRcdFx0XHRcdGxhYmVsU3RyaW5nOiAnVmFsdWUnXG5cdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdHRpY2tzOiB7XG5cdFx0XHRcdFx0XHRcdFx0YmVnaW5BdFplcm86IHRydWUsXG5cdFx0XHRcdFx0XHRcdFx0c3RlcFNpemU6IDEwMDAsXG5cdFx0XHRcdFx0XHRcdFx0c3VnZ2VzdGVkTWF4OiA2MDAwXG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1dXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblxuXHRcdFx0cmV0dXJuIGpzb247ICAgICBcblx0XHR9KTtcbn0pOyJdfQ==
