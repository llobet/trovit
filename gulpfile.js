var 
gulp = require('gulp'),
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
sourcemaps = require('gulp-sourcemaps'),
autoprefixer = require('gulp-autoprefixer'),
sass = require('gulp-sass'),
del = require('del'),
svgo = require('gulp-svgo'),

paths = {
	src : {
		scripts : 'src/js/scripts.js',
		sass: 'src/sass/*.scss',
		bundle: [
			'node_modules/angular/angular.min.js',
			'node_modules/chart.js/dist/Chart.min.js',
			'node_modules/angular-chart/angular-chart.min.js'
		],
		svg: 'src/material/logo.svg'
	},
	public : {
		bundle : 'public/js',
		scripts : 'public/js',
		sass : 'public/css',
		svg: 'public/img'
	},
	clean : {
		bundle : 'public/js/bundle.js',
		scripts : 'public/js/scripts.js',
		sass : 'public/css',
		svg: 'public/img/'
	}
};

gulp.task('clean-script', function() {
  return del([paths.clean.scripts]);
});

gulp.task('clean-sass', function() {
  return del([paths.clean.sass]);
});

gulp.task('clean-bundle', function() {
  return del([paths.clean.bundle]);
});

gulp.task('clean-svg', function() {
  return del([paths.clean.svg]);
});

gulp.task('scripts', ['clean-script'], function() {
	return gulp.src(paths.src.scripts)
		.pipe(sourcemaps.init())
	//	.pipe(uglify())
		.pipe(concat('scripts.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.public.scripts))
});

gulp.task('sass', ['clean-sass'], function() {
	return gulp.src(paths.src.sass)
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(concat('style.css'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.public.sass))
});

gulp.task('bundle', ['clean-bundle'], function() {
	return gulp.src(paths.src.bundle)
		.pipe(sourcemaps.init())
	//	.pipe(uglify())
		.pipe(concat('bundle.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.public.bundle))
});

gulp.task('svg', ['clean-svg'], function() {
    return gulp.src(paths.src.svg)
        .pipe(svgo())
        .pipe(gulp.dest(paths.public.svg));
});

gulp.task('watch', function() {
  gulp.watch(paths.src.scripts, ['scripts']);
  gulp.watch(paths.src.sass, ['sass']);
  gulp.watch(paths.src.svg, ['svg']);
});

gulp.task('clean', ['clean-sass', 'clean-script', 'clean-bundle', 'clean-svg']);
gulp.task('default', ['watch', 'bundle', 'sass', 'scripts', 'svg']);