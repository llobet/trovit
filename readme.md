1. PRACTICAL PART  
In this part of the test we will assess your technical skills and find out more about how you tackle the tasks.
	•You will be in charge of creating a chart of price evolution similar to the attached designs.
	•In attachments you will find 2 designs with information about Trovit, used them as help to solve the task.
	•It is mandatory to use GIT for this assignment.
	•It will be considered a plus if you use the following technologies; NPM, RequireJS, ChartJS, BootstrapSASS, SASS, Grunt, SVG, PHP 3.
	•Once you are done. Upload your repo to bitbucket (as private repo) and share the link with us

2. QUESTIONS 
The second part is set up with a couple of questions related to the role. The questions should be answered in your own words; we do not seek answers from the Internet.
	•Describe your workflow when you create a web page.
	•What UI, Security, Performance, SEO, Maintainability or Technology considerations do you have in mind while building a web application or a site?
	•Traditionally, why has it been better to serve site assets from multiple domains?
	•What's your favourite feature of Internet Explorer? 
	•Explain,"hoisting" in JavaScript.
	•Explain some of the pros and cons for CSS animations versus JavaScript animations. 
	•Please send us your responses by email. There is no time limit to complete the test, but we recommend you to not spend more than 3 hours on the whole test. The test will serve as an overview to see how you work and how you think, so if you don't finish it, don't worry, just send us what you got and we will have a look at the results.