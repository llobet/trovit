/* globals angular, Chart*/
var App = angular.module("app", []);

App.controller("dataCtrl", function ($scope, $http) {
	$scope.json = [];
	$scope.months = [];
	$scope.price_m2s = [];

	$scope.data = $http.get('js/data.json')
		.then(function(res){
			var json = angular.fromJson(res.data);
			var titleChart = '\u20AC / m2 Barcelona';

			angular.forEach(json, function(v, k) {
				$scope.json.push({key:k,year:v.year,month:v.month,price_m2:v.price_m2})
				$scope.months.push(v.month);
				$scope.price_m2s.push(v.price_m2);
			});


			new Chart(document.getElementById("line"), {
				type: 'line',
				data: {
					labels: $scope.months,
					datasets: [{ 
						data: $scope.price_m2s,
						label: titleChart,
						borderColor: "rgba(62, 149, 205, 1)",
						backgroundColor: "rgba(62, 149, 205, 0.5)",
						borderWidth: 3,
						fill: true
					}]
				},
				options: {
					title: {
						display: false,
						text: titleChart
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					responsive: true,
					maintainAspectRatio: false,
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: false,
								labelString: 'Month'
							},
							ticks: {
								maxRotation:90,
								minRotation:45
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: false,
								labelString: 'Value'
							},
							ticks: {
								beginAtZero: true,
								stepSize: 1000,
								suggestedMax: 6000
							}
						}]
					}
				}
			});

			return json;     
		});
});